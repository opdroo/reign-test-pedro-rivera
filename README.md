# Reign Test API

Hi, I'm Pedro Rivera and this project is a small API example with the following Stack:

- NodeJS
- TypeScript
- NestJS
- PostgreSQL
- TypeORM
- Jest
- Docker
- Swagger
- JWT
